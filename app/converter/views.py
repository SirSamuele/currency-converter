# View
from django.http import HttpResponse, JsonResponse
from django.views.generic import TemplateView

# Models
from .models import Rate
from django.db.models import Max

# Utility
from datetime import datetime
from .utils import update_db


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


class ConverterView(TemplateView):
    template_name = "converter/converter.html"

    def get_context_data(self, form=None, errors=None, **kwargs):
        context = super().get_context_data(**kwargs)

        context['currencies'] = Rate.get_currencies(Rate)
        # update_database()
        if form:
            context['form'] = form
        else:
            latest_date = Rate.objects.aggregate(Max('date'))['date__max']
            result = format(round(Rate.objects.get(
                date=latest_date,
                currency='USD'
                ).rate, 2), '.2f')
            if latest_date != datetime.today():
                notes = 'Latest ({})'.format(latest_date.strftime('%Y-%m-%d'))
            else:
                notes = None

            context['form'] = {
                'src_currency': 'EUR',
                'dest_currency': 'USD',
                'amount': '1',
                'result': result,
                'notes': notes
            }

        return context

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            src_currency = request.POST.get('src_currency')
            dest_currency = request.POST.get('dest_currency')
            amount = request.POST.get('amount')
            date = datetime.strptime(request.POST.get('date'), '%Y-%m-%d') if request.POST.get('date') else None
            latest_date = Rate.objects.aggregate(Max('date'))['date__max']


            errors = None
            form = {
                'src_currency': src_currency,
                'dest_currency': dest_currency,
                'amount': amount,
                'date': date if date else latest_date,
                'notes': None if (date or (latest_date == datetime.today)) else 'Latest ({})'.format(latest_date.strftime('%Y-%m-%d'))
            }

            result = convert_currency(src_currency, dest_currency, amount, date if date else latest_date)
            if result['success']:
                form['result'] = format(result['data']['result'], '.2f')
            else:
                form['errors'] = result['data']

        return super(ConverterView, self).render_to_response(self.get_context_data(form=form, errors=errors))
    

def api_converter(request):
    if request.method == 'GET':
        errors = []
        src_currency = request.GET.get('src_currency', None)
        dest_currency = request.GET.get('dest_currency', None)
        amount = request.GET.get('amount', 1)
        date = request.GET.get('date', None)

        date = datetime.strptime(date, '%Y-%m-%d') if date else Rate.objects.aggregate(Max('date'))['date__max']

        # check errors
        if not src_currency:
            errors.append('No src_currency parameter found')
        if not dest_currency:
            errors.append('No dest_currency parameter found')

        if len(errors):
            return JsonResponse({'errors': errors})
        
        result = convert_currency(src_currency, dest_currency, amount, date)
        if result['success']:
            return JsonResponse({
                "amount": result['data']['result'],
                "currency": dest_currency
            })
        else:
            return JsonResponse({'errors': result['data']})

    return JsonResponse({'errors': ['Post request not supported']})


def convert_currency(src_currency='EUR', dest_currency='USD', amount=1, date=datetime.now()):
    errors = []
    data = None

    # update DB; it would not take long if there are not many days of data to load.
    update_db()

    try:
        src_rate = 1 if src_currency == 'EUR' else Rate.objects.get(date=date, currency=src_currency).rate
        dest_rate = 1 if dest_currency == 'EUR' else Rate.objects.get(date=date, currency=dest_currency).rate
        data = {
            'amount': amount,
            'result': round((float(amount)*dest_rate)/src_rate, 2)
        }
    except: 
        errors.append('There is no information available for this date')


    success = not len(errors)
    return {
        'success': success,
        'data': data if success else errors
    }
