from django.urls import path

from . import views

urlpatterns = [
    path('', views.ConverterView.as_view(), name='converter'),
    path('converter/', views.api_converter, name='api-converter'),
]