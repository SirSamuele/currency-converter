from django.contrib import admin

from .models import Rate

class RateAdmin(admin.ModelAdmin):
    list_display = ('date', 'currency', 'rate')
    list_filter = ('date', 'currency')
    search_fields = ['date', 'currency']

admin.site.register(Rate, RateAdmin)