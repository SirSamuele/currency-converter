from django.db import models
from django.db.models import Count


class Rate(models.Model):
    currency = models.CharField(max_length=3)
    rate = models.FloatField()    # REAL type in SQLite
    date = models.DateField()

    def __str__(self):
        return '[{}] {}: {}'.format(self.date, self.currency, self.rate)

    def get_currencies(self):
        return self.objects.values('currency').annotate(n=Count('currency')).order_by('currency')