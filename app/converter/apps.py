from django.apps import AppConfig
from django.db.utils import OperationalError


class ConverterConfig(AppConfig):
    name = 'converter'
    
    def ready(self):
        from .models import Rate
        from datetime import datetime

        from django.db.models import Max
        import xml.etree.ElementTree as ET
        import urllib.request
        url_response = urllib.request.urlopen('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml')
        xml_content = url_response.read()
        root = ET.fromstring(xml_content)

        try:
            print('\n-------------------------------------------------------\nUpdating database... this tasks may require few minutes')
            last_update = Rate.objects.aggregate(Max('date'))['date__max']
            if last_update:
                print('Last update: {}'.format(last_update.strftime('%Y-%m-%d')))

            for date in root[2]:
                rate_date = date.get('time')
                for curr in date:
                    rate_curr = curr.get('currency')
                    rate_rate = curr.get('rate')
                    rate_date_datetime = datetime.strptime(rate_date, '%Y-%m-%d').date()

                    # print('{} < {}'.format(last_update, rate_date_datetime))
                    if not last_update or last_update < rate_date_datetime:
                        rate = Rate(
                            date=rate_date_datetime,
                            currency=rate_curr,
                            rate=float(rate_rate))
                        rate.save()
            print('DONE!\n')
        except OperationalError:
            pass


