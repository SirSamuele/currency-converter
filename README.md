## A simple currency converter that offer a REST API service

This web app consist in a brower interface to calculate the convertion rate based on currencies and date and a corrispective API interface to handle HTTP GET requests.

Used technologies:
- Python 3.6.9 (Django 3.0.3)
- Pip 20.0.2
- Pipenv 2018.11.26

---

**Setup environment and run project**

The project is boxed in a `pipenv` environmant, to guarantee the its portability.
Assuming you are using a Linux distro, you need to follow these steps to get started.

- Install `pip`
```shell
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
```

- Install `pipenv`
```shell
pip install pipenv
```

- Clone the repository
```shell
cd path/to/project
git clone git@bitbucket.org:SirSamuele/currency-converter.git
```

- Run the virtual envirnoment
```shell
cd currency-converter/
pipenv shell
pipenv sync
```

- Apply the migrations to the database, create superuser and run the server
```shell
cd app/
python manage.py migrate
python manage.py createsuperuser
<!-- This operation may require some time, because the app see that the DB is ready and will fill it with the avilable excange rates information -->
<!-- follow the instruction -->
python manage.py runserver
```

---

**The app**

Once you run the server, you can reach it at localhost:8000/

The homepage and only page of the project is a simple form that hallow you to enter the source and destination currencies, the amount of money to convert and the date in wich rate your are interested.

The Database will be filled at the start of the serevr and during every request the source will be checked for possible updates.
Every information about exchanges rates is read from https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml

**Web API**

There is also an API the will handle HTTP GET requests.
You can reach it at localhost:8000/convert and it accept the following parameters:

- ​`amount`: the amount to convert (e.g. 12.35). Default value: 1.00
- `src​_currency`: ISO currency code for the source currency to convert (e.g. EUR, USD, GBP)
- `des​t_currency`: ISO currency code for the destination currency to convert (e.g. EUR, USD, GBP)
- `re​ference_date`: reference date for the exchange rate, in YYYY-MM-DD format. Default value: latest date avilable.

`src_currency` and `dest_currency` are the only required parameters.

The response will be i JSON format and depend on the status of the request.
If the task is successfully completed, the result will be like this:
```json
{
    “amount”: 20.23,
    “currency”: ”EUR”
}
```

If there are problems with the request, they will be listed in the result:
```json
{
    "errors": ["No src_currency parameter found"]
}
```

**Admin**
From here you can acces the admin panel to check all the data evailable in the database: localhost:8000/admin
To access you need to login as superuser, so with the credentials used during the setup of the project.

---

**Credits**

This app was developed by me on my own.
[Samuele Marchisio](https://www.linkedin.com/in/samuele-marchisio-b24553199)